package miumiu.core.models.footer;

import com.day.cq.wcm.api.Page;
import miumiu.core.models.Utils.Constants;
import miumiu.core.models.Utils.ModelHelper;
import miumiu.core.models.bean.Category;
import miumiu.core.models.bean.SocialFooter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = SlingHttpServletRequest.class)
public class FooterModel {
    
    @SlingObject
    private Resource currentResource;
    
    @Inject
    private ResourceResolver resourceResolver;
    
    @Inject
    private Page currentPage;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FooterModel.class);
    private boolean isConfigured = false;
    
    private String pivaText;
    private String copyrightText;
    private String imageFooterPath;
    private String newsLetterText;
    private String newsLetterTitle;
    private String newsPlaceHolder;
    private String bgColor;
    private String fontColor;
    
    private List<Category> categoryList; 
    private List<SocialFooter> socilaList;
    
    @PostConstruct
    protected void init() {
        LOGGER.info(">>>>>>>>>>>>>>> INIT FooterModel >>>>>>>>>>>>>>");
        setPivaText(ModelHelper.getStringProperty(Constants.FOOTER_NODE_COMPONENT, Constants.FOOTER_PROP_PIVA, currentResource, currentPage, resourceResolver));
        setCopyrightText(ModelHelper.getStringProperty(Constants.FOOTER_NODE_COMPONENT, Constants.FOOTER_PROP_COPY, currentResource, currentPage, resourceResolver));
        setImageFooterPath(ModelHelper.getStringProperty(Constants.FOOTER_NODE_COMPONENT, Constants.FOOTER_PROP_IMG_PATH, currentResource, currentPage, resourceResolver));
        setNewsLetterText(ModelHelper.getStringProperty(Constants.FOOTER_NODE_COMPONENT, Constants.FOOTER_PROP_NEWS_LET_TEXT, currentResource, currentPage, resourceResolver));
        setNewsLetterTitle(ModelHelper.getStringProperty(Constants.FOOTER_NODE_COMPONENT, Constants.FOOTER_PROP_NEWS_LET_TITLE, currentResource, currentPage, resourceResolver));
        setNewsPlaceHolder(ModelHelper.getStringProperty(Constants.FOOTER_NODE_COMPONENT, Constants.FOOTER_PROP_NEWS_PLACEHOLDER, currentResource, currentPage, resourceResolver));
        setBgColor(ModelHelper.getStringProperty(Constants.FOOTER_NODE_COMPONENT, Constants.PROP_BG_COLOR, currentResource, currentPage, resourceResolver));
        setFontColor(ModelHelper.getStringProperty(Constants.FOOTER_NODE_COMPONENT, Constants.PROP_FONT_COLOR, currentResource, currentPage, resourceResolver));
        setCategoryList(ModelHelper.getCategoryList(currentResource, currentPage, resourceResolver));
        setSocilaList(ModelHelper.getSocilaList(currentResource, currentPage, resourceResolver));
        if(categoryList.size() > 0 || socilaList.size() > 0){
            setIsConfigured(true);
        }
        LOGGER.info(">>>>>>>>>>>>>>> END INIT FooterModel >>>>>>>>>>>>>>");
    }

    public boolean isIsConfigured() {
        return isConfigured;
    }

    public void setIsConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }

    public String getPivaText() {
        return pivaText;
    }

    public void setPivaText(String pivaText) {
        this.pivaText = pivaText;
    }

    public String getCopyrightText() {
        return copyrightText;
    }

    public String getImageFooterPath() {
        return imageFooterPath;
    }

    public String getNewsLetterText() {
        return newsLetterText;
    }

    public String getNewsLetterTitle() {
        return newsLetterTitle;
    }

    public void setCopyrightText(String copyrightText) {
        this.copyrightText = copyrightText;
    }

    public void setImageFooterPath(String imageFooterPath) {
        this.imageFooterPath = imageFooterPath;
    }

    public void setNewsLetterText(String newsLetterText) {
        this.newsLetterText = newsLetterText;
    }

    public void setNewsLetterTitle(String newsLetterTitle) {
        this.newsLetterTitle = newsLetterTitle;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public List<SocialFooter> getSocilaList() {
        return socilaList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public void setSocilaList(List<SocialFooter> socilaList) {
        this.socilaList = socilaList;
    }

    public String getNewsPlaceHolder() {
        return newsPlaceHolder;
    }

    public void setNewsPlaceHolder(String newsPlaceHolder) {
        this.newsPlaceHolder = newsPlaceHolder;
    }

    public String getBgColor() {
        return bgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }


    
    
    
}
