package miumiu.core.models.bean;

/**
 *
 * @author Giuseppe
 */
public class CategoryElement {
    
    private String categoryName;
    private String linkName;
    private String linkPath;

    public String getCategoryName() {
        return categoryName;
    }

    public String getLinkName() {
        return linkName;
    }

    public String getLinkPath() {
        return linkPath;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public void setLinkPath(String linkPath) {
        this.linkPath = linkPath;
    }
    
    

}
