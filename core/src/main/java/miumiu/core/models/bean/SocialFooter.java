package miumiu.core.models.bean;

/**
 *
 * @author Giuseppe
 */
public class SocialFooter {
    
    private String path;
    private String altImage;
    private String href;
    private String title;

    public String getPath() {
        return path;
    }

    public String getAltImage() {
        return altImage;
    }

    public String getHref() {
        return href;
    }

    public String getTitle() {
        return title;
    }

    

    public void setPath(String path) {
        this.path = path;
    }

    public void setAltImage(String altImage) {
        this.altImage = altImage;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    
    
    

}
