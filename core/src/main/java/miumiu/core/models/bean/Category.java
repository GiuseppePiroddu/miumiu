package miumiu.core.models.bean;

import java.util.List;

/**
 *
 * @author Giuseppe
 */
public class Category {
    
    private String categoryName;
    private List<CategoryElement> categoryElements;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<CategoryElement> getCategoryElements() {
        return categoryElements;
    }

    public void setCategoryElements(List<CategoryElement> categoryElements) {
        this.categoryElements = categoryElements;
    }
    
    

}
