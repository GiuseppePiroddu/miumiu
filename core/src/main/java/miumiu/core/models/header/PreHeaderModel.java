package miumiu.core.models.header;

import com.day.cq.wcm.api.Page;
import miumiu.core.models.Utils.Constants;
import miumiu.core.models.Utils.ModelHelper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author Giuseppe
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class PreHeaderModel {
    
    @SlingObject
    private Resource currentResource;
    
    @Inject
    private ResourceResolver resourceResolver;
    
    @Inject
    private Page currentPage;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PreHeaderModel.class);
    private boolean isConfigured = false;
    
    private String storeText;
    private String storePath;
    private String freeShippingText;
    private String shippingToText;
    private String helpYouText;
    private String bgColor;
    private String fontColor;
    private String mayWeHelpNumber;
    private String mayWeHelpOpeningTime;
    private String linkToContactUsPage;
    private String currentCountry;
    private String currentLanguage;
            
    
    
    @PostConstruct
    protected void init() {
        LOGGER.info(">>>>>>>>>>>>>>> INIT PreHeaderModel >>>>>>>>>>>>>>");
        setStoreText(ModelHelper.getStringProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.HEADER_PROP_STORE_TEXT, currentResource, currentPage, resourceResolver));
        setStorePath(ModelHelper.getStringPathProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.HEADER_PROP_STORE_PATH, currentResource, currentPage, resourceResolver));
        setFreeShippingText(ModelHelper.getStringProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.HEADER_PROP_FREE_SHIPPING_TEXT, currentResource, currentPage, resourceResolver));
        setShippingToText(ModelHelper.getStringProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.HEADER_PROP_SHIPPING_TO_TEXT, currentResource, currentPage, resourceResolver));
        setHelpYouText(ModelHelper.getStringProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.HEADER_PROP_HELP_TEXT, currentResource, currentPage, resourceResolver));
        setBgColor(ModelHelper.getStringProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.PROP_BG_COLOR, currentResource, currentPage, resourceResolver));
        setFontColor(ModelHelper.getStringProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.PROP_FONT_COLOR, currentResource, currentPage, resourceResolver));
        
        setCurrentCountry(ModelHelper.getCurrentCountry(currentPage));
        setCurrentLanguage(ModelHelper.getCurrentLanguage(currentPage));
        
        setMayWeHelpNumber(ModelHelper.getStringProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.HEADER_PROP_MAY_H_NUMBER, currentResource, currentPage, resourceResolver));
        setMayWeHelpOpeningTime(ModelHelper.getStringProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.HEADER_PROP__MAY_H_OPT, currentResource, currentPage, resourceResolver));
        setLinkToContactUsPage(ModelHelper.getStringPathProperty(Constants.PREHEADER_NODE_COMPONENT, Constants.HEADER_PROP_MAY_H_CON_LINK, currentResource, currentPage, resourceResolver));
        LOGGER.info(">>>>>>>>>>>>>>> END INIT PreHeaderModel >>>>>>>>>>>>>>");
        
    }

    public boolean isIsConfigured() {
        return isConfigured;
    }

    public String getStoreText() {
        return storeText;
    }

    public String getStorePath() {
        return storePath;
    }

    public String getFreeShippingText() {
        return freeShippingText;
    }

    public String getShippingToText() {
        return shippingToText;
    }

    public String getHelpYouText() {
        return helpYouText;
    }

    public String getBgColor() {
        return bgColor;
    }

    public String getFontColor() {
        return fontColor;
    }

    public String getMayWeHelpNumber() {
        return mayWeHelpNumber;
    }

    public String getMayWeHelpOpeningTime() {
        return mayWeHelpOpeningTime;
    }

    public String getLinkToContactUsPage() {
        return linkToContactUsPage;
    }

    public String getCurrentCountry() {
        return currentCountry;
    }

    public String getCurrentLanguage() {
        return currentLanguage;
    }
    
          
    public void setIsConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }

    public void setStoreText(String storeText) {
        this.storeText = storeText;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }

    public void setFreeShippingText(String freeShippingText) {
        this.freeShippingText = freeShippingText;
    }

    public void setShippingToText(String shippingToText) {
        this.shippingToText = shippingToText;
    }

    public void setHelpYouText(String helpYouText) {
        this.helpYouText = helpYouText;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public void setMayWeHelpNumber(String mayWeHelpNumber) {
        this.mayWeHelpNumber = mayWeHelpNumber;
    }

    public void setMayWeHelpOpeningTime(String mayWeHelpOpeningTime) {
        this.mayWeHelpOpeningTime = mayWeHelpOpeningTime;
    }

    public void setLinkToContactUsPage(String linkToContactUsPage) {
        this.linkToContactUsPage = linkToContactUsPage;
    }

    public void setCurrentCountry(String currentCountry) {
        this.currentCountry = currentCountry;
    }

    public void setCurrentLanguage(String currentLanguage) {
        this.currentLanguage = currentLanguage;
    }
    
    

}
