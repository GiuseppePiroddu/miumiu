package miumiu.core.models.header;

import com.day.cq.wcm.api.Page;
import miumiu.core.models.Utils.ExternalLinkUtils;
import miumiu.core.models.Utils.ModelHelper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = SlingHttpServletRequest.class)
public class CountrySelector {

    private static final String PROP_LINK_TITLE = "linkTitle";
    private static final String PROP_LINK_URL = "linkUrl";
    private static final String COUNTRY_SELECTOR_ITEMS = "items";
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CountrySelector.class);

    @Inject
    private Resource resource;

    @Inject
    private Page currentPage;

    @Inject
    private ResourceResolver resourceResolver;
    
    private Map<String, String> countries;

    private String currentCountry;

    @PostConstruct
    public void init() {
        LOGGER.info("Activate of {}", resource.getPath());
        Page countryPage = ModelHelper.getCountryPage(currentPage);
        currentCountry = ModelHelper.getCurrentCountry(currentPage);
        setCountryList(countryPage);
    }

    private void setCountryList(Page countryPage) {
        countries = new TreeMap<>(Comparator.naturalOrder());
        Resource countrySelectorResource = countryPage.getContentResource(COUNTRY_SELECTOR_ITEMS);
        getPropertiesCountrySelector(countrySelectorResource);
    }

    private void getPropertiesCountrySelector(Resource countrySelectorResource) {
        if (countrySelectorResource != null && !ResourceUtil.isNonExistingResource(countrySelectorResource)) {
            Iterator<Resource> resources = countrySelectorResource.listChildren();
            while (resources.hasNext()) {
                Resource resource = resources.next();
                final String linkTitle = ModelHelper.getStringProperty(resource,PROP_LINK_TITLE,null);
                if (linkTitle != null) {
                    final String linkUrl = ModelHelper.getStringProperty(resource, PROP_LINK_URL, null);
                    if (linkUrl != null) {
                        countries.put(linkTitle, ExternalLinkUtils.getInternalOrExternalLink(resourceResolver, linkUrl));
                    } else {
                        LOGGER.warn("Impossible to add resource ({}) to list because property {} is missing", PROP_LINK_URL, resource);
                    }
                } else {
                    LOGGER.warn("Impossible to add resource ({}) to list because property {} is missing", PROP_LINK_TITLE, resource);
                }
            }
        } else {
            LOGGER.warn("Impossible to add compute list because input resource is null or not existent");
        }
    }

    public Map<String, String> getCountries() {
        return countries;
    }

    public String getCurrentCountry() {
        return currentCountry;
    }
    
}