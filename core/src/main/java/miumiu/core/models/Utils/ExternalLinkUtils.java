package miumiu.core.models.Utils;

import com.day.cq.commons.Externalizer;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Check if a link is external or internal
 * in case of internal add .html as suffix
 */
public class ExternalLinkUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalLinkUtils.class);

    public static String getInternalOrExternalLink(ResourceResolver resourceResolver, String value) {
        String result = value;
        if (!StringUtils.isEmpty(value) && hasNode(resourceResolver, value)) {
            result = value.concat(".html");
        }
        if (StringUtils.isEmpty(value)) {
            result = "#";
        }
        return result;
    }

    private static boolean hasNode(ResourceResolver resourceResolver, String value) {
        return resourceResolver.getResource(value) != null;
    }

    /***
     *
     * @param externalizer externalizer
     * @param resourceResolver resourceResolver
     * @param path path to be mapped
     * @param domain externalizer domain property
     * @return mapped path
     */
    public static String mappingUrl(final Externalizer externalizer, final ResourceResolver resourceResolver, final String path, final String domain) {
        String uri = StringUtils.EMPTY;
        if (externalizer != null && resourceResolver != null) {
            try {
                uri = externalizer.externalLink(resourceResolver, domain, StringUtils.appendIfMissing(path, Constants.EXTENSION_HTML));
            } catch (IllegalArgumentException e) {
                LOGGER.error("Externalizer error: {}", e.getMessage());
            }
        } else {
            LOGGER.error("Externalizer and Resource Resolver cannot be null");
        }
        return uri;
    }
}
