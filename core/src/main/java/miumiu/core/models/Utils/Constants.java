package miumiu.core.models.Utils;

/**
 *
 * @author Giuseppe
 */
public final class Constants {
    
    //FOOTER
    public static final String FOOTER_NODE_COMPONENT = "/jcr:content/component-footer";
    public static final String FOOTER_NODE_CATEGOTY_SECTION = "categorySection";
    public static final String FOOTER_NODE_CATEGOTY_ELEMENTS = "categoryElements";
    
    public static final String FOOTER_NODE_ELEMENTS_NAME = "linkName";
    public static final String FOOTER_NODE_ELEMENTS_PATH = "linkPath";
    
    
    public static final String FOOTER_NODE_CATEGOTY_NAME = "categoryName";
    public static final String FOOTER_NODE_SOCIAL_SECTION = "footerSocialLink";
    public static final String FOOTER_SOCIAL_PROP_PATH = "path";
    public static final String FOOTER_SOCIAL_PROP_ALT = "altImage";
    public static final String FOOTER_SOCIAL_PROP_HREF = "href";
    public static final String FOOTER_SOCIAL_PROP_TITLE = "title";
    
    public static final String FOOTER_PROP_PIVA = "pivaText";
    public static final String FOOTER_PROP_COPY = "copyrightText";
    public static final String FOOTER_PROP_IMG_PATH = "imageFooterPath";
    public static final String FOOTER_PROP_NEWS_LET_TEXT = "newsLetterText";
    public static final String FOOTER_PROP_NEWS_LET_TITLE = "newsLetterTitle";
    public static final String FOOTER_PROP_NEWS_PLACEHOLDER = "newsPlaceHolder";
    
    
    //HEADER
    public static final String PREHEADER_NODE_COMPONENT = "/jcr:content/component-preheader";
    public static final String HEADER_PROP_STORE_TEXT = "storeText";
    public static final String HEADER_PROP_STORE_PATH = "storePath";
    public static final String HEADER_PROP_FREE_SHIPPING_TEXT = "freeShippingText";
    public static final String HEADER_PROP_SHIPPING_TO_TEXT = "shippingToText";
    public static final String HEADER_PROP_HELP_TEXT = "helpYouText";
    
    public static final String HEADER_PROP_MAY_H_NUMBER = "mayWeHelpNumber";
    public static final String HEADER_PROP__MAY_H_OPT = "mayWeHelpOpeningTime";
    public static final String HEADER_PROP_MAY_H_CON_LINK = "linkToContactUsPage";
    
    public static final String EXTENSION_HTML = ".html";
    
    //GENERIC
    public static final String PROP_BG_COLOR = "bgColor";
    public static final String PROP_FONT_COLOR = "fontColor";

}
