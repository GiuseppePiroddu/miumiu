package miumiu.core.models.Utils;


import com.day.cq.wcm.api.Page;
import miumiu.core.models.bean.Category;
import miumiu.core.models.bean.CategoryElement;
import miumiu.core.models.bean.SocialFooter;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
/**
 *
 * @author Giuseppe
 */
public class ModelHelper {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelHelper.class);
    
    public static Page getCountryPage(Page currentPage){
        if(currentPage != null && currentPage.getDepth() > 2){
            return currentPage.getAbsoluteParent(2);
        }
        return null;
    }
    
    public static Page getLanguagePage(Page currentPage){
        if(currentPage != null && currentPage.getDepth() > 3){
            return currentPage.getAbsoluteParent(3);
        }
        return null;
    }
    
    public static String getCurrentCountry(Page currentPage){
        String currentConuntry = StringUtils.EMPTY;
        Page countryPage = getCountryPage(currentPage);
        if(countryPage != null){
            return countryPage.getTitle();
        }
        return currentConuntry;
    }
    
    public static String getCurrentLanguage(Page currentPage){
        String currentLanguage = StringUtils.EMPTY;
        Page languagePage = getLanguagePage(currentPage);
        if(languagePage != null){
            Locale locale = new Locale(languagePage.getLanguage(false).toString(),getCurrentCountry(currentPage));
            return locale.getLanguage();
        }
        return currentLanguage;
    }
    
    public static String getSimpleUrl(String path) {
        String resultLink = StringUtils.EMPTY;
        if (path != null && !path.equals("")) {
            resultLink = path + ".html";
        }
        return resultLink;
    }
    
    public static List<CategoryElement> getCategoryElements(Resource currentResource, Page currentPage, ResourceResolver resourceResolver, String categoryName) {
         List<CategoryElement> elementList = new ArrayList<>();
         if(currentResource != null && currentResource.hasChildren()){
             Resource elemntsResource = currentResource.getChild(Constants.FOOTER_NODE_CATEGOTY_ELEMENTS);
             if(elemntsResource != null){
                 Iterator<Resource> elements = elemntsResource.listChildren();
                 while (elements.hasNext()) {
                     Resource elementsItem = elements.next();
                     if(elementsItem != null){
                         CategoryElement categoryElement = new CategoryElement();
                         categoryElement.setCategoryName(getStringProperty(Constants.FOOTER_NODE_CATEGOTY_NAME, elementsItem));
                         categoryElement.setLinkName(getStringProperty(Constants.FOOTER_NODE_ELEMENTS_NAME, elementsItem));
                         categoryElement.setLinkPath(getSimpleUrl(resourceResolver.map(getStringProperty(Constants.FOOTER_NODE_ELEMENTS_PATH, elementsItem))));                       
                         if(categoryElement.getCategoryName().equalsIgnoreCase(categoryName)) elementList.add(categoryElement);
                     }                     
                 }
             }else if(currentPage != null){
                Page languagePage = getLanguagePage(currentPage);
                Resource languageResourceFooter = resourceResolver != null ? resourceResolver.getResource(languagePage.getPath() + Constants.FOOTER_NODE_COMPONENT ) : null;
                elementList = getCategoryElemnts(languageResourceFooter,categoryName, resourceResolver);
            }
             
         }else if(currentPage != null){
                Page languagePage = getLanguagePage(currentPage);
                Resource languageResourceFooter = resourceResolver != null ?  resourceResolver.getResource(languagePage.getPath() + Constants.FOOTER_NODE_COMPONENT ) : null;
                elementList = getCategoryElemnts(languageResourceFooter,categoryName, resourceResolver);
            }
         
         return elementList;
    }
    
    public static List<CategoryElement> getCategoryElemnts(Resource languageResourceFooter, String categoryName, ResourceResolver resourceResolver) {
        List<CategoryElement> elementList = new ArrayList<>();
         if(languageResourceFooter != null && languageResourceFooter.hasChildren()){
             Resource elemntsResource = languageResourceFooter.getChild(Constants.FOOTER_NODE_CATEGOTY_ELEMENTS);
             if(elemntsResource != null){
                 Iterator<Resource> elements = elemntsResource.listChildren();
                 while (elements.hasNext()) {
                     Resource elementsItem = elements.next();
                     if(elementsItem != null){
                         CategoryElement categoryElement = new CategoryElement();
                         categoryElement.setCategoryName(getStringProperty(Constants.FOOTER_NODE_CATEGOTY_NAME, elementsItem));
                         categoryElement.setLinkName(getStringProperty(Constants.FOOTER_NODE_ELEMENTS_NAME, elementsItem));
                         categoryElement.setLinkPath(getSimpleUrl(resourceResolver.map(getStringProperty(Constants.FOOTER_NODE_ELEMENTS_PATH, elementsItem))));                       
                         if(categoryElement.getCategoryName().equalsIgnoreCase(categoryName)) elementList.add(categoryElement);
                     }                     
                 }
             }             
         }
         return elementList;
    }
    
    public static List<Category> getCategoryList(Resource currentResource, Page currentPage, ResourceResolver resourceResolver) {
         List<Category> categoryList = new ArrayList<>();
         if(currentResource != null && currentResource.hasChildren()){
             Resource categoryResource = currentResource.getChild(Constants.FOOTER_NODE_CATEGOTY_SECTION);
             if(categoryResource != null){
                 Iterator<Resource> categories = categoryResource.listChildren();
                 while (categories.hasNext()) {
                     Resource categoryItem = categories.next();
                     if(categoryItem != null){
                         Category category = new Category();
                         category.setCategoryName(getStringProperty(Constants.FOOTER_NODE_CATEGOTY_NAME, categoryItem));
                         category.setCategoryElements(getCategoryElements(currentResource, currentPage, resourceResolver, category.getCategoryName()));
                         categoryList.add(category);
                     }                     
                 }
             }else if(currentPage != null){
                Page languagePage = getLanguagePage(currentPage);
                Resource languageResourceFooter = resourceResolver != null ? resourceResolver.getResource(languagePage.getPath() + Constants.FOOTER_NODE_COMPONENT ) : null;
                categoryList = getCategoryList(languageResourceFooter, resourceResolver);
            }
             
         }else if(currentPage != null){
                Page languagePage = getLanguagePage(currentPage);
                Resource languageResourceFooter = resourceResolver != null ?  resourceResolver.getResource(languagePage.getPath() + Constants.FOOTER_NODE_COMPONENT ) : null;
                categoryList = getCategoryList(languageResourceFooter, resourceResolver);
            }
         
         return categoryList;
    }
    
    public static List<Category> getCategoryList(Resource languageResourceFooter, ResourceResolver resourceResolver) {
        List<Category> categoryList = new ArrayList<>();
         if(languageResourceFooter != null && languageResourceFooter.hasChildren()){
             Resource categoryResource = languageResourceFooter.getChild(Constants.FOOTER_NODE_CATEGOTY_SECTION);
             if(categoryResource != null){
                 Iterator<Resource> categories = categoryResource.listChildren();
                 while (categories.hasNext()) {
                     Resource categoryItem = categories.next();
                     if(categoryItem != null){
                         Category category = new Category();
                         category.setCategoryName(getStringProperty(Constants.FOOTER_NODE_CATEGOTY_NAME, categoryItem));
                         category.setCategoryElements(getCategoryElemnts(languageResourceFooter, category.getCategoryName(), resourceResolver));
                         categoryList.add(category);
                     }                     
                 }
             }             
         }
         return categoryList;
    }
    
    public static List<SocialFooter> getSocilaList(Resource currentResource, Page currentPage, ResourceResolver resourceResolver) {
        List<SocialFooter> socilaList = new ArrayList<>();
         if(currentResource != null && currentResource.hasChildren()){
             Resource socialResource = currentResource.getChild(Constants.FOOTER_NODE_SOCIAL_SECTION);
             if(socialResource != null){
                 Iterator<Resource> socials = socialResource.listChildren();
                 while (socials.hasNext()) {
                     Resource socialItem = socials.next();
                     if(socialItem != null){
                         SocialFooter socialFooter = new SocialFooter();
                         socialFooter.setPath(getStringProperty(Constants.FOOTER_SOCIAL_PROP_PATH, socialItem));
                         socialFooter.setAltImage(getStringProperty(Constants.FOOTER_SOCIAL_PROP_ALT, socialItem));
                         socialFooter.setHref(getStringProperty(Constants.FOOTER_SOCIAL_PROP_HREF, socialItem));
                         socialFooter.setTitle(getStringProperty(Constants.FOOTER_SOCIAL_PROP_TITLE, socialItem));
                         socilaList.add(socialFooter);
                     }                     
                 }
             }else if(currentPage != null){
                Page languagePage = getLanguagePage(currentPage);
                Resource languageResourceFooter = resourceResolver != null ? resourceResolver.getResource(languagePage.getPath() + Constants.FOOTER_NODE_COMPONENT ) : null;
                socilaList = getSocilaList(languageResourceFooter);
            }
             
         }else if(currentPage != null){
                Page languagePage = getLanguagePage(currentPage);
                Resource languageResourceFooter = resourceResolver != null ? resourceResolver.getResource(languagePage.getPath() + Constants.FOOTER_NODE_COMPONENT ) : null;
                socilaList = getSocilaList(languageResourceFooter);
            }
         
         return socilaList;
    }
    
    public static List<SocialFooter> getSocilaList(Resource marketResourceFooter) {
        List<SocialFooter> socilaList = new ArrayList<>();
         if(marketResourceFooter != null && marketResourceFooter.hasChildren()){
             Resource socialResource = marketResourceFooter.getChild(Constants.FOOTER_NODE_SOCIAL_SECTION);
             if(socialResource != null){
                 Iterator<Resource> socials = socialResource.listChildren();
                 while (socials.hasNext()) {
                     Resource socialItem = socials.next();
                     if(socialItem != null){
                         SocialFooter socialFooter = new SocialFooter();
                         socialFooter.setPath(getStringProperty(Constants.FOOTER_SOCIAL_PROP_PATH, socialItem));
                         socialFooter.setAltImage(getStringProperty(Constants.FOOTER_SOCIAL_PROP_ALT, socialItem));
                         socialFooter.setHref(getStringProperty(Constants.FOOTER_SOCIAL_PROP_HREF, socialItem));
                         socialFooter.setTitle(getStringProperty(Constants.FOOTER_SOCIAL_PROP_TITLE, socialItem));
                         socilaList.add(socialFooter);
                     }                     
                 }
             }             
         }
         return socilaList;
    }
    
    public static String getStringProperty(String component, String propertyName, Resource currentResource, Page currentPage, ResourceResolver resourceResolver) {
        Node node = currentResource != null ? currentResource.adaptTo(Node.class) : null;
        try {            
            if(node != null && node.hasProperty(propertyName) && !node.getProperty(propertyName).getString().equals("")){
                return node.getProperty(propertyName).getString();
            }else if(currentPage != null){
                Page languagePage = getLanguagePage(currentPage);
                Resource languageResourceFooter = resourceResolver != null ? resourceResolver.getResource(languagePage.getPath() + component ) : null;
                return getStringProperty(propertyName, languageResourceFooter);
            }
        } catch (RepositoryException ex) {
            LOGGER.error(ex.getMessage());
        }
        return StringUtils.EMPTY;
    }
    
    public static  String getStringProperty(String propertyName, Resource marketresourceFooter) {
        Node node = marketresourceFooter != null ? marketresourceFooter.adaptTo(Node.class) : null;
        try {            
            if(node != null && node.hasProperty(propertyName) && !node.getProperty(propertyName).getString().equals("")){
                return node.getProperty(propertyName).getString();
            }
        } catch (RepositoryException ex) {
            LOGGER.error(ex.getMessage());
        }
        return StringUtils.EMPTY;
    }
    
    public static String getStringPathProperty(String component, String propertyName, Resource currentResource, Page currentPage, ResourceResolver resourceResolver) {
        Node node = currentResource != null ? currentResource.adaptTo(Node.class) : null;
        try {            
            if(node != null && node.hasProperty(propertyName) && !node.getProperty(propertyName).getString().equals("")){
                return getSimpleUrl(node.getProperty(propertyName).getString());
            }else if(currentPage != null){
                Page languagePage = getLanguagePage(currentPage);
                Resource languageResourceFooter = resourceResolver != null ? resourceResolver.getResource(languagePage.getPath() + component ) : null;
                return getStringPathProperty(propertyName, languageResourceFooter);
            }
        } catch (RepositoryException ex) {
            LOGGER.error(ex.getMessage());
        }
        return StringUtils.EMPTY;
    }
    
    public static  String getStringPathProperty(String propertyName, Resource marketresourceFooter) {
        Node node = marketresourceFooter != null ? marketresourceFooter.adaptTo(Node.class) : null;
        try {            
            if(node != null && node.hasProperty(propertyName) && !node.getProperty(propertyName).getString().equals("")){
                return getSimpleUrl(node.getProperty(propertyName).getString());
            }
        } catch (RepositoryException ex) {
            LOGGER.error(ex.getMessage());
        }
        return StringUtils.EMPTY;
    }
    
    public static String getStringProperty(Node currentNode, String propertyName, String defaultValue) {
        String value = defaultValue;
        try {
            if (currentNode != null) {
                value = JcrUtils.getStringProperty(currentNode, propertyName, defaultValue);
            }
        } catch (Exception e) {
            // ignore
        }
        return value;
    }
    
    public static String getStringProperty(Resource resource, String propertyName, String defaultValue) {
        return getStringProperty(resource == null ? null : resource.adaptTo(Node.class), propertyName, defaultValue);
    }

}
