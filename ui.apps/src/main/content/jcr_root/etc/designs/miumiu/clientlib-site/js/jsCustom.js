/* variabile globale che indica l elemento che è visualizzato nello slider del menu */
indice = 0;
elemMenu = 0;
index = 0;
numElem = 0;

$(document).on("click" , ".o-header .o-bgWhite .o-frecce .c-btn.right" , function(e){
	slideNext();
});

$(document).on("click" , ".o-header .o-bgWhite .o-frecce .c-btn.left" , function(e){
	slidePrev();
});

$(document).on("click" , ".o-header .o-menu-first-level .c-menu-ul .c-elem-menu" , function(e){
	openCloseHeaderMenu(e , this);
});

$(document).on("mouseover" , ".o-header .o-bgWhite .o-center-carousel .o-container-menu-second-level .c-menu-second-level .c-submenu .c-submenu-ul .c-submeni-li" , function(e){
    showImage(e , this);
});

function showImage(event , nodo){
    
    var elem = $(nodo).get(0),
        start = elem.getAttribute("data-index")
        radice = $(nodo).closest(".c-menu-second-level.active"),
        imgs = $(radice).find("img.c-img-second-level"),
        img = $(imgs).get(start);

    $(imgs).removeClass("is-img-active");
    $(img).addClass("is-img-active");

}

function settings(){
    elemMenu = $(".o-header .o-menu-header .o-menu-first-level .o-menu .o-nav-menu .c-menu-ul .c-elem-menu"),
    numElem = $(elemMenu).length,
    index = numElem - 1;
}

/* slide carousel */
function slideNext(){
	$("#myCarousel").carousel("next");
	$("#myCarousel").carousel("pause");

    if(indice < index){
        indice++;
    }else{
        indice = 0;
    }

    var indexPrev = indice ==  0 ? index : (indice - 1),
        indexNext = indice >= index ? 0 : (indice + 1),
        liCurrent = $(elemMenu).get(indice),
        liPrev = $(elemMenu).get(indexPrev),
        elemPrev = $(liPrev).find(".c-label-menu"),
        newLabelPrev = $(elemPrev).text(),
        liNext = $(elemMenu).get(indexNext),
        elemNext = $(liNext).find(".c-label-menu"),
        newLabelNext = $(elemNext).text();

    $("#prevButton > .c-text-label").html(newLabelPrev);
    $("#nextButton > .c-text-label").html(newLabelNext);

    $(elemMenu).removeClass("js-selected-item");
    $(liCurrent).addClass("js-selected-item");
}

function slidePrev(){
	$("#myCarousel").carousel("prev");
	$("#myCarousel").carousel("pause");

    if(indice > 0){
        indice--;
    }else{
        indice = index;
    }

    var indexPrev = indice < 1 ? index : (indice - 1),
        indexNext = indice ==  (numElem - 1) ? 0 : (indice + 1),
        liPrev = $(elemMenu).get(indexPrev),
        liCurrent = $(elemMenu).get(indice),
        elemPrev = $(liPrev).find(".c-label-menu"),
        newLabelPrev = $(elemPrev).text(),
        liNext = $(elemMenu).get(indexNext),
        elemNext = $(liNext).find(".c-label-menu"),
        newLabelNext = $(elemNext).text();

    $("#prevButton > .c-text-label").html(newLabelPrev);
    $("#nextButton > .c-text-label").html(newLabelNext);

    $(elemMenu).removeClass("js-selected-item");
    $(liCurrent).addClass("js-selected-item");
    
}

/* apre e chiude il menu header*/
function openCloseHeaderMenu(event , nodo){

    settings();

    var elem = $(nodo).get(0),
        start = elem.getAttribute("data-index"),
        startIndex = parseInt(start),
        prev = 0,
        next = 0,
        nextLabel,
        prevLabel;

    /* calcolo indici per prev e next */
    if(startIndex == 0){
        next = startIndex + 1;
        prev = numElem - 1;
    }else if(startIndex > 0 && startIndex < (numElem - 1) ){
        next = startIndex + 1;
        prev = startIndex - 1;
    }else if(startIndex == (numElem - 1)){
        next = 0;
        prev = startIndex - 1;
    }

    /* settaggio label per bottoni next e prev */
    var elem = $(elemMenu).get(next),
        containerLabel = $(elem).find(".c-label-menu"),
        nextLabel = $(containerLabel).html();

    $("#nextButton > .c-text-label").html(nextLabel);

    elem = $(elemMenu).get(prev),
    containerLabel = $(elem).find(".c-label-menu"),
    prevLabel = $(containerLabel).html();

    $("#prevButton > .c-text-label").html(prevLabel);

    /* aggiornamento indice di partenza*/
    indice = startIndex;

    /* apertura chiusura menu */
	if($(nodo).hasClass("js-selected-item")){
		$("#header").removeClass("js-open-menu");
		$(nodo).removeClass("js-selected-item");
	}else{
		$(".o-menu-first-level .c-elem-menu").removeClass("js-selected-item");
		$(nodo).addClass("js-selected-item");

		if(!$("#header").hasClass("js-open-menu")){
			$("#header").addClass("js-open-menu");
		}

        $("#myCarousel").carousel(startIndex);
        $("#myCarousel").carousel("pause");
	}
}